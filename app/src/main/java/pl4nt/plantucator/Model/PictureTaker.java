package pl4nt.plantucator.Model;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.core.content.FileProvider;

// Author: Brigham Moll
// Script: PictureTaker.java
// Description: Allows the user to take photos from within the app.
// Created on 11/20/2019
public class PictureTaker
{
    // REFERENCE: https://developer.android.com/training/camera/photobasics#java

    // Constants

    public static final int REQUEST_IMAGE_CAPTURE = 1;

    // Variables

    // The last saved photo's file path.
    private static String currentPhotoPath = "";

    // Starts the process of taking a photo. Pass in the current activity.
    // Also pass in the file for saving.--> from createImageFile()
    // Handle photo capture completion in onActivityResult().
    // Use "if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK)"
    // to see if photo has been taken.
    public static void takePhoto(Activity activity, File photoFile) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            // Create the File where the photo should go
//            File photoFile = null;
//            try {
//                photoFile = createImageFile(activity);
//            } catch (IOException ex) {
//                // Error occurred while creating the File...
//            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(activity,
                        "pl4nt.plantucator.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                activity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    // Prepares file for image capture. Pass in current activity. Returns image file.
    public static File createImageFile(Activity activity) throws IOException
    {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    // Returns the path of the last photo taken.
    public static String getLastTakenPhotoPath()
    {
        return currentPhotoPath;
    }
}
