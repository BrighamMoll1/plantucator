package pl4nt.plantucator.Model;

import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.support.common.FileUtil;
import org.tensorflow.lite.support.common.ops.NormalizeOp;
import org.tensorflow.lite.support.image.ImageProcessor;
import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.image.ops.ResizeOp;
import org.tensorflow.lite.support.model.Model;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;
import org.tensorflow.lite.support.common.FileUtil;
import org.tensorflow.lite.support.common.TensorProcessor;
import org.tensorflow.lite.support.label.TensorLabel;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.util.List;
import java.util.Map;

// Author: Brigham Moll
// Script: TensorflowImageProcessor.java
// Description: Processes images using a Tensorflow Lite Object Detection model. (.tflite)
// Created on 11/20/2019
public class TensorflowImageProcessor
{
    // Constants

    // Where the model file is found.
    private static final String MODEL_PATH = "mobilenet_v1_1.0_224_quant.tflite";

    // Where the label file for the model is found.
    private static final String LABELS_PATH = "labels_mobilenet_quant_v1_224.txt";

    // Methods

    // Analyzes a given Bitmap image.
    // activity parameter --> Pass in the activity currently in use.
    public static String analyzeImage(Activity activity, Bitmap imageToProcess)
    {
        // REFERENCE: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/lite/experimental/support/java/README.md
        // Initialize the image processor.
        ImageProcessor imageProcessor =
            new ImageProcessor.Builder()
                    .add(new ResizeOp(224, 224, ResizeOp.ResizeMethod.BILINEAR))
                    .build();

        TensorImage tImage = new TensorImage(DataType.UINT8);

        // Preprocess image.
        tImage.load(imageToProcess);
        tImage = imageProcessor.process(tImage);

        // Make a container for the result.
        TensorBuffer probabilityBuffer =
                TensorBuffer.createFixedSize(new int[]{1, 1001}, DataType.UINT8);

        // Initialize model.
        try{
            MappedByteBuffer tfliteModel
                    = FileUtil.loadMappedFile(activity,
                    MODEL_PATH);
            Interpreter tflite = new Interpreter(tfliteModel);

            // Run the model.
            if(null != tflite)
                tflite.run(tImage.getBuffer(), probabilityBuffer.getBuffer());

        } catch (IOException e){
            Log.e("tfliteSupport", "Error reading model", e);
        }

        List<String> associatedAxisLabels = null;

        // Load label file.
        try{
            associatedAxisLabels = FileUtil.loadLabels(activity, LABELS_PATH);
        } catch (IOException e){
            Log.e("tfliteSupport", "Error reading label file", e);
        }

        // Post-processor which dequantize the result
        TensorProcessor probabilityProcessor =
                new TensorProcessor.Builder().add(new NormalizeOp(0, 255)).build();

        Map<String, Float> floatMap = null;
        if (null != associatedAxisLabels) {
            // Map of labels and their corresponding probability
            TensorLabel labels = new TensorLabel(associatedAxisLabels,
                    probabilityProcessor.process(probabilityBuffer));

            // Create a map to access the result based on label
            floatMap = labels.getMapWithFloatValue();
        }

        // Go through the result map and decide what seems most likely to be in this photo that was analyzed.
        String result = decideResult(floatMap);

        return result;
    }

    // Looks through a given float map from a Tensorflow Lite analysis and finds the most likely result.
    private static String decideResult(Map<String, Float> givenFloatMap)
    {
        String leadingResult = "";

        float highestVal = 0;

        // Find highest value of confidence, return the string name associated with it.
        for(Map.Entry<String,Float> entry : givenFloatMap.entrySet())
        {
            if(entry.getValue() > highestVal)
            {
                highestVal = entry.getValue();
                leadingResult = entry.getKey();
            }
        }

        return leadingResult;
    }
}
