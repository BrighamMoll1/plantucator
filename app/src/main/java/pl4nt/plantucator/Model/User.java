package pl4nt.plantucator.Model;

public class User {

    // Role strings from account login info.
    public final static String STUDENT_ROLE_STRING = "Student";
    public final static String HOBBYIST_ROLE_STRING = "Hobbyist";
    public final static String EDUCATOR_ROLE_STRING = "Educator";

    private String username;
    private String email;
    private String password;
    private String role;

    public User(String username, String email, String password, String role){
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public User() {}


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
