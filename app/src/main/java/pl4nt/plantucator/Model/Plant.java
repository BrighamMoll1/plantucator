package pl4nt.plantucator.Model;

import com.google.gson.annotations.SerializedName;

public class Plant {
    private int id;
    private String name;
    private String type;
    private Boolean danger;

    @SerializedName("body")
    private String information;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Boolean getDanger() {
        return danger;
    }

    public String getInformation() {
        return information;
    }
}
