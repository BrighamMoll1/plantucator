package pl4nt.plantucator;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class StudentActivitiesQuizzesActivity extends AppCompatActivity {

    // UI Elements
    private Button btnActivity1;

    // Reference to this activity.
    private Activity thisActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_activities_quizzes);

        // Set activity reference.
        thisActivity = this;

        // Get ui element references.
        btnActivity1 = findViewById(R.id.btn_act_1);

        // Set activity start listener.
        btnActivity1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Go to the student activity page.
                Intent intent = new Intent(thisActivity, ActivityCompletionActivity.class);
                startActivity(intent);
            }
        });
    }
}
