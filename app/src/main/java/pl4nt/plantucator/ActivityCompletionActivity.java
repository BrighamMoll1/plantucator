package pl4nt.plantucator;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import pl4nt.plantucator.Model.PictureTaker;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;

public class ActivityCompletionActivity extends AppCompatActivity {

    // Constants

    // Total ImageButtons, used or not.
    public final static int MAX_IMG_BTNS = 6;


    // Variables

    // Activity information.
    private String activityName = "Activity 1";
    private String plantName = "wall clock";
    private int plantQuantity = 3;

    // UI Elements
    private TextView txtActivityTitle;
    private TextView txtActivityGoal;
    private Button btnSubmitActivity;
    private ImageButton[] btnImgListPhotoTaking;

    // Reference to this activity.
    private Activity thisActivity;

    // The last image button pressed.
    private int lastImgBtnPressedIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completion);

        // Set activity reference.
        thisActivity = this;

        // Get UI references.
        btnImgListPhotoTaking = new ImageButton[6];
        btnImgListPhotoTaking[0] = findViewById(R.id.btn_img_photo_1);
        btnImgListPhotoTaking[1] = findViewById(R.id.btn_img_photo_2);
        btnImgListPhotoTaking[2] = findViewById(R.id.btn_img_photo_3);
        btnImgListPhotoTaking[3] = findViewById(R.id.btn_img_photo_4);
        btnImgListPhotoTaking[4] = findViewById(R.id.btn_img_photo_5);
        btnImgListPhotoTaking[5] = findViewById(R.id.btn_img_photo_6);

        txtActivityTitle = findViewById(R.id.txt_activity_name);
        txtActivityGoal = findViewById(R.id.txt_activity_task);
        btnSubmitActivity = findViewById(R.id.btn_submit_activity);

        // Set text for activity.
        txtActivityTitle.setText(activityName);
        String activityGoal = "Find " + plantQuantity + " of " + plantName + "!";
        txtActivityGoal.setText(activityGoal);
        // Set listeners on buttons being used for photo-taking, make others invisible.
        for(int iPhotoTakingImageButton = 0; iPhotoTakingImageButton < MAX_IMG_BTNS; iPhotoTakingImageButton++)
        {
            if(iPhotoTakingImageButton >= plantQuantity)
            {
                // Unused buttons, disable and make them do nothing.
                btnImgListPhotoTaking[iPhotoTakingImageButton].setEnabled(false);
                btnImgListPhotoTaking[iPhotoTakingImageButton].setVisibility(View.GONE);
                continue;
            }

            final int currentImgBtnIndex = iPhotoTakingImageButton;

            btnImgListPhotoTaking[iPhotoTakingImageButton].setOnClickListener(new View.OnClickListener() {

                private int imageButtonIndex = currentImgBtnIndex;

                @Override
                public void onClick(View view) {
                    startPhotoTaking(imageButtonIndex);
                }
            });
        }

        // Enable button when all photos ready, disable for now.
        btnSubmitActivity.setEnabled(false);
    }

    // Starts photo taking process. Pass in which ImageButton was pressed.
    private void startPhotoTaking(int photoSlot)
    {
        lastImgBtnPressedIndex = photoSlot;

        try {
            // Prepare an image file.
            File savedPicture = PictureTaker.createImageFile(thisActivity);

            // Try to take a photo, handle the rest in onActivityResult().
            PictureTaker.takePhoto(thisActivity, savedPicture);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // If a photo has just been taken, handle the photo.
        if (requestCode == PictureTaker.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK)
        {
            // Fetch the image that was captured, as a Bitmap.
            Bitmap bitmapImage = BitmapFactory.decodeFile(PictureTaker.getLastTakenPhotoPath());

            // Display image captured in correct button image.
            btnImgListPhotoTaking[lastImgBtnPressedIndex].setImageBitmap(bitmapImage);
        }
    }
}
