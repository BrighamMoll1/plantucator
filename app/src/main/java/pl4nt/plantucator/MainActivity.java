package pl4nt.plantucator;

import androidx.appcompat.app.AppCompatActivity;
import pl4nt.plantucator.Model.User;

import android.os.Bundle;
import android.content.Intent;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    //database
    FirebaseDatabase database;
    DatabaseReference users;

    EditText edtUser;
    EditText edtPass;
    Button btnSignIn, btnToRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        //firebase database
        FirebaseApp.initializeApp(this);
        database = FirebaseDatabase.getInstance();

        //
        users = database.getReference("Users");

        edtUser = findViewById(R.id.edtUser);
        edtPass = findViewById(R.id.edtPass);


        btnSignIn = findViewById(R.id.btnSignIn);
        btnToRegister = findViewById(R.id.btnToRegister);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btnSignIn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                signIn(edtUser.getText().toString(),
                        edtPass.getText().toString());
            }

        });

        btnToRegister.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void onClick(View view){
                Intent s  = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(s);
            }
        });


    }

    private void signIn(final String user, final String password){
        users.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.child(user).exists()){
                    if(!user.isEmpty()){
                        User login = dataSnapshot.child(user).getValue(User.class);

                        if(login.getPassword().equals(password) && login.getRole().equals("Hobbyist")){

                            //test
                            Toast.makeText(MainActivity.this, "Hello Hobbyist", Toast.LENGTH_SHORT).show();

                            Intent s  = new Intent(getApplicationContext(), MenuActivity.class);
                            s.putExtra("LoginUser", login.getUsername());
                            s.putExtra("LoginEmail", login.getEmail());
                            s.putExtra("LoginPass", login.getPassword());
                            s.putExtra("LoginRole", login.getRole());
                            startActivity(s);

                        } else if(login.getPassword().equals(password) && login.getRole().equals("Educator")){

                            //test
                            Toast.makeText(MainActivity.this, "Hello Educator", Toast.LENGTH_SHORT).show();

                            Intent s  = new Intent(getApplicationContext(), MenuActivity.class);
                            s.putExtra("LoginUser", login.getUsername());
                            s.putExtra("LoginEmail", login.getEmail());
                            s.putExtra("LoginPass", login.getPassword());
                            s.putExtra("LoginRole", login.getRole());
                            startActivity(s);
                        } else if(login.getPassword().equals(password) && login.getRole().equals("Student")){

                            //test
                            Toast.makeText(MainActivity.this, "Hello Student", Toast.LENGTH_SHORT).show();

                            Intent s  = new Intent(getApplicationContext(), MenuActivity.class);
                            s.putExtra("LoginUser", login.getUsername());
                            s.putExtra("LoginEmail", login.getEmail());
                            s.putExtra("LoginPass", login.getPassword());
                            s.putExtra("LoginRole", login.getRole());
                            startActivity(s);
                        } else{
                            Toast.makeText(MainActivity.this, "Incorrect Password", Toast.LENGTH_LONG).show();
                        }
                    }


                } else {
                    Toast.makeText(MainActivity.this, "User is not registered", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

}
