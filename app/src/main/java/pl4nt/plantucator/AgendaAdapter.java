package pl4nt.plantucator;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

public class AgendaAdapter extends RecyclerView.Adapter<AgendaAdapter.AgendaViewHolder> {
    private String[] aDataset;

    public static class AgendaViewHolder extends RecyclerView.ViewHolder{

        public TextView txtView;

        public AgendaViewHolder(TextView a){
            super(a);
            txtView = a;
        }
    }

    public AgendaAdapter(String[] agendaDataset){
        aDataset = agendaDataset;
    }

    @Override
    public AgendaAdapter.AgendaViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType){
        TextView a = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.atextview, parent, false);

        AgendaViewHolder ah = new AgendaViewHolder(a);
        return ah;

    }

    @Override
    public void onBindViewHolder(AgendaViewHolder holder, int position){

        holder.txtView.setText(aDataset[position]);
    }

    @Override
    public int getItemCount(){
        return aDataset.length;
    }

}
