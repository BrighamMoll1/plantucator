package pl4nt.plantucator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import pl4nt.plantucator.Model.User;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class RegisterActivity extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference users;

    EditText edtUser, edtEmail, edtPass;
    Button  btnRegister;

    Spinner spnRole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);


        //firebase database
        database = FirebaseDatabase.getInstance();
        users= database.getReference("Users");

        edtUser = findViewById(R.id.edtUser);
        edtEmail = findViewById(R.id.edtEmail);
        edtPass = findViewById(R.id.edtPass);

        btnRegister = findViewById(R.id.btnRegister);

        spnRole = findViewById(R.id.spnRole);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.user_role, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spnRole.setAdapter(adapter);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final User user = new User(edtUser.getText().toString(),
                        edtEmail.getText().toString(),
                        edtPass.getText().toString(),
                        spnRole.getSelectedItem().toString());

                users.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.child(user.getUsername()).exists()){
                            Toast.makeText(RegisterActivity.this, "User exists", Toast.LENGTH_SHORT).show();
                        }else {
                            users.child(user.getUsername()).setValue(user);
                            Toast.makeText(RegisterActivity.this, "New User added", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        //TODO: Add later
                    }
                });

                Intent s  = new Intent(getApplicationContext(), MenuActivity.class);
                s.putExtra("LoginUser", edtUser.getText().toString());
                s.putExtra("LoginEmail", edtEmail.getText().toString());
                s.putExtra("LoginPass", edtPass.getText().toString());
                s.putExtra("LoginRole", spnRole.getSelectedItem().toString());
                startActivity(s);
            }
        });

    }
}
