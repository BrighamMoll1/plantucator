package pl4nt.plantucator;

import androidx.appcompat.app.AppCompatActivity;
import pl4nt.plantucator.Model.PictureTaker;
import pl4nt.plantucator.Model.TensorflowImageProcessor;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Map;

public class InformationRetrievalActivity extends AppCompatActivity {

    // Where the picture that was taken will be displayed.
    private ImageView imgPictureResult = null;

    // Where the name of the picture will be displayed.
    private TextView txtPictureName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_retrieval);

        // Fetch references.
        imgPictureResult = findViewById(R.id.img_picture_result);
        txtPictureName = findViewById(R.id.txt_name_result);

        // Fetch the image that was captured, as a Bitmap.
        Bitmap bitmapImage = BitmapFactory.decodeFile(PictureTaker.getLastTakenPhotoPath());

        // Analyze the image with Tensorflow Lite.
        String resultPrediction = TensorflowImageProcessor.analyzeImage(this, bitmapImage);

        // Display result of name of thing in picture.
        txtPictureName.setText(resultPrediction);

        // Set the image in the ImageView to be the captured image.
        imgPictureResult.setImageBitmap(bitmapImage);
    }
}
