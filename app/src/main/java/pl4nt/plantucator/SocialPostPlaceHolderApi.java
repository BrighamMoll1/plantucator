package pl4nt.plantucator;
import java.util.List;

import pl4nt.plantucator.Model.Plant;
import retrofit2.Call;
import retrofit2.http.GET;

public interface SocialPostPlaceHolderApi {

    //json array
    @GET("api/Plants")
    Call<List<Plant>> GetPlants();
}
