package pl4nt.plantucator;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.view.View;

public class PlantAgendaActivity extends AppCompatActivity {

    private RecyclerView rcyAgenda;
    private RecyclerView.Adapter aAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agenda);


        rcyAgenda = findViewById(R.id.rcyAgenda);
        rcyAgenda.setHasFixedSize(true);

        //using linear layout manager
        layoutManager = new LinearLayoutManager(this);
        rcyAgenda.setLayoutManager(layoutManager);

        //aAdapter = new AgendaAdapter(agendaDataset);
        rcyAgenda.setAdapter(aAdapter);




    }

}
