package pl4nt.plantucator;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import pl4nt.plantucator.Model.PictureTaker;
import pl4nt.plantucator.Model.User;
import pl4nt.plantucator.R;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ImageButton;

import java.io.File;
import java.io.IOException;

import static pl4nt.plantucator.Model.User.EDUCATOR_ROLE_STRING;
import static pl4nt.plantucator.Model.User.HOBBYIST_ROLE_STRING;
import static pl4nt.plantucator.Model.User.STUDENT_ROLE_STRING;

public class MenuActivity extends AppCompatActivity {

    // Constants

    TextView txtUser;
    Button btnSocial, btn_option1, btn_option2;
    public final static int CAMERA_PERMISSION_REQUEST = 123;

    // Text for buttons that change depending on roles.
    public final static String STUDENT_ACT_QUIZ_BTN_STRING = "Quizzes and Activities";
    public final static String STUDENT_JOIN_CLASS_BTN_STRING = "Join Class";
    public final static String HOBBYIST_AGENDA_BTN_STRING = "Plant Agenda";
    public final static String HOBBYIST_PLANTCARE_TIPS_BTN_STRING = "Plant Care Tips";
    public final static String EDUCATOR_CREATE_QUIZ_BTN_STRING = "Create Quizzes and Activities";
    public final static String EDUCATOR_MANAGE_CLASS_BTN_STRING = "Manage Class";

    // Variables

    ImageButton btnSettings;

    // Button for image capture and analysis.
    private ImageButton btnImgCapture = null;

    // Button for Completing activities and quizzes as a student.
    private Button btnActQuiz = null;

    // Reference to the activity.
    private Activity thisActivity;

    // Methods


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);

        txtUser = findViewById(R.id.txtUser);

        final String logUser = getIntent().getStringExtra("LoginUser");
        final String logEmail = getIntent().getStringExtra("LoginEmail");
        final String logPass = getIntent().getStringExtra("LoginPass");
        final String logRole = getIntent().getStringExtra("LoginRole");

        txtUser.setText("Welcome " + logRole + " " + logUser);

        thisActivity = this;

        // References to things in UI.
        btnSettings = findViewById(R.id.btnSettings);
        btnImgCapture = findViewById(R.id.btnImageCapture);

        btnSocial = findViewById(R.id.btnSocial);

        // If student, set up option 2 button to be for completing activities and quizzes.
        if(logRole != null)
        {
            if (logRole.equals(STUDENT_ROLE_STRING) ) {

                prepareStudentActivitiesQuizzesBtn();
            }

            if(logRole.equals(HOBBYIST_ROLE_STRING)){

                prepareHobbyistButtons();

            }

            if(logRole.equals(EDUCATOR_ROLE_STRING)){

                prepareEducatorButtons();

            }
        }
        else
        {

            prepareStudentActivitiesQuizzesBtn();
        }



        // Listeners.
        btnSettings.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent s = new Intent(getApplicationContext(), SettingsActivity.class);
                s.putExtra("LoginUser", logUser);
                s.putExtra("LoginEmail", logEmail);
                s.putExtra("LoginPass", logPass);
                s.putExtra("LoginRole", logRole);
                startActivity(s);
            }
        });



        // Makes the Image Capture button start the photo taking process.
        btnImgCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    // Prepare an image file.
                    File savedPicture = PictureTaker.createImageFile(thisActivity);

                    // Try to take a photo, handle the rest in onActivityResult().
                    PictureTaker.takePhoto(thisActivity, savedPicture);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        btnSocial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s = new Intent(getApplicationContext(), SocialActivity.class);
                startActivity(s);
            }
        });

        // Ask for camera permission.
        if (ContextCompat.checkSelfPermission(thisActivity, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted, so ask for permission.
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // If a photo has just been taken, handle the photo.
        if (requestCode == PictureTaker.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK)
        {
            // Go to the information retrieval page.
            Intent intent = new Intent(this, InformationRetrievalActivity.class);
            startActivity(intent);
        }
    }

    // Prepares the student activity/quizzes button in the main menu.
    private void prepareStudentActivitiesQuizzesBtn()
    {
        btnActQuiz = findViewById(R.id.btn_option2);
        btn_option1 = findViewById(R.id.btn_option1);
        btn_option1.setText(STUDENT_JOIN_CLASS_BTN_STRING);
        btnActQuiz.setText(STUDENT_ACT_QUIZ_BTN_STRING);


        btnActQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Go to the activities and quizzes page.
                Intent intent = new Intent(thisActivity, StudentActivitiesQuizzesActivity.class);
                startActivity(intent);
            }
        });
    }
    private void prepareHobbyistButtons()
    {
        //hobbyist buttons
        btn_option1 = findViewById(R.id.btn_option1);
        btn_option2 = findViewById(R.id.btn_option2);
        btn_option1.setText(HOBBYIST_AGENDA_BTN_STRING);
        btn_option2.setText(HOBBYIST_PLANTCARE_TIPS_BTN_STRING);

    }

    private void prepareEducatorButtons()
    {
        //educator buttons
        btn_option1 = findViewById(R.id.btn_option1);
        btn_option2 = findViewById(R.id.btn_option2);
        btn_option1.setText(EDUCATOR_MANAGE_CLASS_BTN_STRING);
        btn_option2.setText(EDUCATOR_CREATE_QUIZ_BTN_STRING);

    }
}
