package pl4nt.plantucator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import pl4nt.plantucator.Model.User;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SettingsActivity extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference users;

    EditText edtNewUser, edtNewEmail, edtNewPass;

    Spinner spnNewRole;

    Button btnSave, btnLogOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        //firebase database
        FirebaseApp.initializeApp(this);
        database = FirebaseDatabase.getInstance();

        //initialize
        users = database.getReference("Users");

        edtNewUser = findViewById(R.id.edtNewUser);
        edtNewEmail = findViewById(R.id.edtNewEmail);
        edtNewPass = findViewById(R.id.edtNewPass);

        spnNewRole = findViewById(R.id.spnNewRole);

        btnSave = findViewById(R.id.btnSave);
        btnLogOut = findViewById(R.id.btnLogOut);


        final String logOldUser = getIntent().getStringExtra("LoginUser");
        final String logOldEmail = getIntent().getStringExtra("LoginEmail");
        final String logOldPass = getIntent().getStringExtra("LoginPass");
        final String logOldRole = getIntent().getStringExtra("LoginRole");

        edtNewUser.append(logOldUser);
        edtNewEmail.append(logOldEmail);
        edtNewPass.append(logOldPass);
        spnNewRole.setSelected(Boolean.parseBoolean(logOldRole));

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAccountInfo(logOldUser,
                        edtNewUser.getText().toString(),
                        edtNewEmail.getText().toString(),
                        edtNewPass.getText().toString(),
                        spnNewRole.getSelectedItem().toString());
            }
        });

        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent s = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(s);
            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.user_role, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spnNewRole.setAdapter(adapter);


    }

    private void setAccountInfo(final String logOldUser, final String username, final String email, final String password, final String role){
        users.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User oldInfo = dataSnapshot.child(logOldUser).getValue(User.class);

                //changing user name doesn't work
                if (username != oldInfo.getUsername()){
                    oldInfo.setUsername(username);
                    //users.child(String.valueOf(users.child(logOldUser))).setValue(username);
                }
                if(email != oldInfo.getEmail()){
                    oldInfo.setEmail(email);
                }
                if(password != oldInfo.getPassword()){
                    oldInfo.setPassword(password);
                }
                if(role != oldInfo.getRole()){
                    oldInfo.setRole(role);
                }

                users.child(logOldUser).setValue(oldInfo);

                Toast.makeText(SettingsActivity.this, "Information Updated", Toast.LENGTH_SHORT).show();

                Intent s = new Intent(getApplicationContext(), MenuActivity.class);
                s.putExtra("LoginUser", oldInfo.getUsername());
                s.putExtra("LoginEmail", oldInfo.getEmail());
                s.putExtra("LoginPass", oldInfo.getPassword());
                s.putExtra("LoginRole", oldInfo.getRole());
                startActivity(s);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
