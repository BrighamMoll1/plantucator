package pl4nt.plantucator;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import pl4nt.plantucator.Model.Plant;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.widget.TextView;

import java.util.List;

public class SocialActivity extends AppCompatActivity {

    private TextView txtPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.social);

        txtPost = findViewById(R.id.txtPost);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://127.0.0.1:44373/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SocialPostPlaceHolderApi socialPostPlaceHolderApi = retrofit.create(SocialPostPlaceHolderApi.class);

        Call<List<Plant>> call = socialPostPlaceHolderApi.GetPlants();

        call.enqueue(new Callback<List<Plant>>() {
            @Override
            public void onResponse(Call<List<Plant>> call, Response<List<Plant>> response) {

                //success check
                if(!response.isSuccessful()){
                    txtPost.setText("Code:" + response.code());
                    return;
                }

                List<Plant> plants = response.body();

                for(Plant plant : plants){
                    String content = "";
                    content += "ID:" + plant.getId() + "\n";
                    content += "Name:" + plant.getName() + "\n";
                    content += "Type:" + plant.getType() + "\n";
                    content += "Danger:" + plant.getDanger() + "\n";
                    content += "Info:" + plant.getInformation() + "\n";

                    txtPost.append(content);
                }


            }

            @Override
            public void onFailure(Call<List<Plant>> call, Throwable t) {
                txtPost.setText(t.getMessage());
            }
        });


    }

}
